# bingo-cloud-kubernetes-scripts



### Argocd Login

```bash
argocd login 192.168.0.162
```

### Argocd Create app
```bash
k apply -f ./bingo-cloud-home/argocd/app.yml
```

### Argocd Apply app

```bash
argocd app sync bingo-cloud-app
```

### Check the list of apps

```bash
argocd app list
argocd app get [app-name]
```